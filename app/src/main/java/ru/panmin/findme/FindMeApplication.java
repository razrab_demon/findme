package ru.panmin.findme;

import android.app.Application;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import ru.panmin.findme.io.DataManager;
import ru.panmin.findme.io.FindMeService;

public class FindMeApplication extends Application {

    public static final int REQ_GOOGLE_SIGN_IN = 201;

    public static final String INTENT_KEY_ROOM_ID = "find_me_room_id";
    public static final String INTENT_KEY_ROOM_NAME = "find_me_room_name";
    public static final EventBus BUS = new EventBus();
    private static FindMeApplication instance;
    private DataManager dataManager;

    public static FindMeApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initDataManager();
        initPrefs();
    }

    private void initDataManager() {
        if (dataManager == null) {
            createDataManager();
        }
    }

    public void createDataManager() {
        dataManager = new DataManager(FindMeService.Creator.newService());
    }

    private void initPrefs() {
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public DataManager getDataManager() {
        return dataManager;
    }

}