package ru.panmin.findme.ui.model;

import java.util.Date;

public interface AuthInterface {

    long getId();

    String getGoogleId();

    String getEmail();

    String getName();

    String getToken();

    String getPhoto();

    double getLat();

    double getLng();

    Date getCreatedAt();

}