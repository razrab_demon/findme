package ru.panmin.findme.ui.base;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.panmin.findme.FindMeApplication;
import ru.panmin.findme.R;
import ru.panmin.findme.io.DataManager;
import ru.panmin.findme.utils.DialogFactory;
import ru.panmin.findme.utils.Utils;
import rx.Subscription;

public abstract class BaseFragment extends Fragment {

    protected DataManager dataManager;
    protected Subscription subscription;
    protected MaterialDialog progressDialog;

    @Nullable
    @BindView(R.id.rootView)
    protected View rootView;

    public BaseFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataManager = FindMeApplication.get().getDataManager();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FindMeApplication.BUS.register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void init();

    @Override
    public void onDestroy() {
        FindMeApplication.BUS.unregister(this);
        super.onDestroy();
    }

    protected void finishActivity() {
        hideKeyboard();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().finishAfterTransition();
        } else {
            getActivity().finish();
        }
    }

    protected void hideKeyboard() {
        View view = getRootView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Nullable
    public View getRootView() {
        return rootView;
    }

    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = DialogFactory.createProgressDialog(getActivity());
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showSnackbar(@NonNull String text) {
        View rootView = getRootView();
        if (rootView != null) {
            Utils.getShortSnackbar(rootView, text).show();
        } else {
            Utils.showShortToast(getActivity(), text);
        }
    }

    public void showSnackbar(@StringRes int text) {
        View rootView = getRootView();
        if (rootView != null) {
            Utils.getShortSnackbar(rootView, text).show();
        } else {
            Utils.showShortToast(getActivity(), text);
        }
    }

    @Subscribe
    public void onEvent(String gag) {
    }

    public interface ToolbarShadowInterface {

        void onHideShadow();

        void onShowShadow();

    }

}