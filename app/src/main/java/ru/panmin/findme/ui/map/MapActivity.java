package ru.panmin.findme.ui.map;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import ru.panmin.findme.FindMeApplication;
import ru.panmin.findme.R;
import ru.panmin.findme.ui.base.BaseToolbarActivity;

public class MapActivity
        extends BaseToolbarActivity
        implements OnMapReadyCallback {

    private GoogleMap googleMap;

    public MapActivity() {
    }

    public static Intent getStartIntent(Context context, long roomId, @NonNull String roomName) {
        Intent intent = new Intent(context, MapActivity.class);
        intent.putExtra(FindMeApplication.INTENT_KEY_ROOM_ID, roomId);
        intent.putExtra(FindMeApplication.INTENT_KEY_ROOM_NAME, roomName);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_maps;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        setTitle(getIntent().getStringExtra(FindMeApplication.INTENT_KEY_ROOM_NAME));
    }

    @Override
    protected void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onHomeClick() {
        finishActivity();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(54.151354, 45.059260));
        builder.include(new LatLng(54.240531, 45.287226));
        LatLngBounds bounds = builder.build();

        googleMap.addMarker(new MarkerOptions().position(new LatLng(54.151354, 45.059260)).draggable(true));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(54.240531, 45.287226)).draggable(true));

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.1);

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.animateCamera(cu);
    }

}