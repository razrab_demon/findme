package ru.panmin.findme.ui.model;

public interface RoomCreatedInterface extends RoomInterface {

    long getUserId();

    boolean isHide();

    double getLat();

    double getLng();

}