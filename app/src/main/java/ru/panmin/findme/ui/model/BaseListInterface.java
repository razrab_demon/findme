package ru.panmin.findme.ui.model;

import java.util.List;

public interface BaseListInterface<T> {

    List<T> getList();

    int getCount();

}