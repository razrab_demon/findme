package ru.panmin.findme.ui.base;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import ru.panmin.findme.R;

public abstract class BaseToolbarActivity extends BaseActivity
        implements BaseFragment.ToolbarShadowInterface {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.toolbarShadow)
    protected View toolbarShadow;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finishActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
    }

    protected abstract void initToolbar();

    protected abstract void onHomeClick();

    @Override
    public void onHideShadow() {
        toolbarShadow.setVisibility(View.GONE);
    }

    @Override
    public void onShowShadow() {
        toolbarShadow.setVisibility(View.VISIBLE);
    }

}