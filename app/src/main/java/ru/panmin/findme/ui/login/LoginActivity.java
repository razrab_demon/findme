package ru.panmin.findme.ui.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import butterknife.BindView;
import butterknife.OnClick;
import ru.panmin.findme.FindMeApplication;
import ru.panmin.findme.R;
import ru.panmin.findme.io.FindMePrefs;
import ru.panmin.findme.io.responses.AuthResponse;
import ru.panmin.findme.ui.base.BaseActivity;
import ru.panmin.findme.ui.room.RoomListActivity;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity
        extends BaseActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.signInButton)
    protected SignInButton signInButton;

    private GoogleApiClient mGoogleApiClient;

    public LoginActivity() {
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_launch;
    }

    @Override
    protected void init() {
        signInButton.setSize(SignInButton.SIZE_WIDE);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @OnClick(R.id.signInButton)
    protected void signIn() {
        showProgress();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, FindMeApplication.REQ_GOOGLE_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
        }

        if (requestCode == FindMeApplication.REQ_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                subscription = dataManager.getFindMeService().auth(acct.getEmail(), acct.getId(), acct.getDisplayName())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Subscriber<AuthResponse>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                hideProgress();
                                showSnackbar(R.string.enter_error);
                            }

                            @Override
                            public void onNext(AuthResponse response) {
                                if (TextUtils.isEmpty(response.getErrorMessage())) {
                                    FindMePrefs.auth(response.getData());
                                    FindMeApplication.get().createDataManager();
                                    startActivity(RoomListActivity.getStartIntent(LoginActivity.this));
                                    hideProgress();
                                    finishActivity();
                                } else {
                                    hideProgress();
                                    showSnackbar(response.getErrorMessage());
                                }

                            }
                        });
            } else {
                hideProgress();
                showSnackbar(R.string.enter_error);
            }
        } else {
            hideProgress();
            showSnackbar(R.string.enter_error);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        hideProgress();
        showSnackbar(R.string.enter_error);
    }

}