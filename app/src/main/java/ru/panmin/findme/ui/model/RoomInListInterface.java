package ru.panmin.findme.ui.model;

public interface RoomInListInterface extends RoomInterface {

    int getPassword();

    boolean isPassword();

    int getCountUser();

}