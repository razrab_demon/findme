package ru.panmin.findme.ui.room;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionMenu;

import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.OnClick;
import ru.panmin.findme.R;
import ru.panmin.findme.io.responses.RoomCreateResponse;
import ru.panmin.findme.io.responses.RoomListResponse;
import ru.panmin.findme.ui.base.BaseToolbarActivity;
import ru.panmin.findme.ui.map.MapActivity;
import ru.panmin.findme.ui.model.RoomInterface;
import ru.panmin.findme.utils.DialogFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RoomListActivity
        extends BaseToolbarActivity
        implements SearchView.OnQueryTextListener,
        RoomListAdapter.OnRoomClickListener {

    @BindView(R.id.swipeRefreshLayoutRooms)
    protected SwipeRefreshLayout swipeRefreshLayoutRooms;

    @BindView(R.id.recyclerViewRooms)
    protected RecyclerView recyclerViewRooms;

    @BindView(R.id.fab)
    protected FloatingActionMenu fab;

    private RoomListAdapter adapter;

    private SearchView searchView;
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_search, menu);

            final MenuItem searchItem = menu.findItem(R.id.action_search);
            searchView = (SearchView) searchItem.getActionView();
            searchView.setOnQueryTextListener(RoomListActivity.this);

            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnCloseListener(() -> true);

            EditText editTextSearchView = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(editTextSearchView, R.drawable.search_view_cursor);
            } catch (Exception ignored) {
            }

            searchView.setQueryHint(getResources().getString(R.string.hint_search_room));

            searchView.setIconified(false);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            searchView = null;
        }
    };

    public RoomListActivity() {
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RoomListActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_list, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                startSupportActionMode(actionModeCallback);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_room_list;
    }

    @Override
    protected void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }
    }

    @Override
    protected void onHomeClick() {
    }

    @Override
    protected void init() {
        initRecyclerView();

        swipeRefreshLayoutRooms.setColorSchemeResources(R.color.color_primary, R.color.color_accent, R.color.color_primary_dark);
        swipeRefreshLayoutRooms.setOnRefreshListener(this::getRoomList);

        getRoomList();
    }

    private void initRecyclerView() {
        recyclerViewRooms.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                    fab.hideMenu(true);
                else if (dy < 0)
                    fab.showMenu(true);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewRooms.setLayoutManager(layoutManager);
        recyclerViewRooms.setItemAnimator(new DefaultItemAnimator());

        adapter = new RoomListAdapter(this);
        recyclerViewRooms.setAdapter(adapter);
    }

    @OnClick(R.id.menu_search)
    protected void searchHiddenRoom() {
    }

    @OnClick(R.id.menu_add)
    protected void addNewRoom() {
        View view = getLayoutInflater().inflate(R.layout.view_room_create, null, false);

        AppCompatEditText editTextRoomName = (AppCompatEditText) view.findViewById(R.id.editTextRoomName);
        TextInputEditText editTextPassword = (TextInputEditText) view.findViewById(R.id.editTextPassword);
        AppCompatCheckBox checkBoxIsHide = (AppCompatCheckBox) view.findViewById(R.id.checkBoxIsHide);

        MaterialDialog.SingleButtonCallback callback = (dialog, which) -> {
            if (TextUtils.isEmpty(editTextRoomName.getText().toString().trim())) {
                showSnackbar(R.string.error_empty_room_name);
                return;
            }

            subscription = dataManager.getFindMeService().createRoom(
                    editTextRoomName.getText().toString().trim(),
                    editTextPassword.getText().toString().trim(),
                    checkBoxIsHide.isChecked())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<RoomCreateResponse>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            swipeRefreshLayoutRooms.setRefreshing(false);
                            hideProgress();
                            showSnackbar(R.string.enter_error);
                        }

                        @Override
                        public void onNext(RoomCreateResponse response) {
                            swipeRefreshLayoutRooms.setRefreshing(false);
                            if (TextUtils.isEmpty(response.getErrorMessage())) {
                                getRoomList();
                                showSnackbar(R.string.room_created);
                            } else {
                                showSnackbar(response.getErrorMessage());
                            }
                        }
                    });
            dialog.dismiss();
        };

        DialogFactory.createRoomCreateDialog(this, view, callback).show();
    }

    private void getRoomList() {
        getRoomList(searchView == null ? "" : searchView.getQuery().toString());
    }

    private void getRoomList(String search) {
        subscription = dataManager.getFindMeService().roomList(search)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RoomListResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        swipeRefreshLayoutRooms.setRefreshing(false);
                        hideProgress();
                        showSnackbar(R.string.enter_error);
                    }

                    @Override
                    public void onNext(RoomListResponse response) {
                        swipeRefreshLayoutRooms.setRefreshing(false);
                        if (TextUtils.isEmpty(response.getErrorMessage())) {
                            adapter.setData(response.getData().getList(), response.getData().getCount());
                        } else {
                            showSnackbar(response.getErrorMessage());
                        }
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getRoomList(newText);
        return true;
    }

    @Override
    public void onRoomClick(RoomInterface room) {
        startActivity(MapActivity.getStartIntent(this, room.getId(), room.getName()));
    }

}