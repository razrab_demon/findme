package ru.panmin.findme.ui.room;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.panmin.findme.R;
import ru.panmin.findme.ui.model.RoomInListInterface;
import ru.panmin.findme.ui.model.RoomInterface;

class RoomListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RoomInListInterface> items = new ArrayList<>();
    private int totalItems;

    private OnRoomClickListener onRoomClickListener;

    RoomListAdapter(OnRoomClickListener onRoomClickListener) {
        this.onRoomClickListener = onRoomClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RoomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_room, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((RoomViewHolder) holder).bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private RoomInterface getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    void setData(List<? extends RoomInListInterface> items, int total) {
        this.items.clear();
        this.items.addAll(items);
        this.totalItems = total;
        notifyDataSetChanged();
    }

    void addData(List<? extends RoomInListInterface> items, int total) {
        int position = items.size();
        this.items.addAll(items);
        this.totalItems = total;
        this.notifyItemInserted(position);
    }

    public interface OnRoomClickListener {
        void onRoomClick(RoomInterface room);
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        protected AppCompatTextView text;

        RoomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(RoomInListInterface room) {
            text.setText(
                    room.getId() + "\n" +
                            room.getName() + "\n" +
                            room.getPassword() + "\n" +
                            room.getUpdatedAt() + "\n" +
                            room.getCountUser()
            );
            itemView.setOnClickListener(v -> onRoomClickListener.onRoomClick(room));
        }

    }

}