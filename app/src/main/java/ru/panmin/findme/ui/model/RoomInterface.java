package ru.panmin.findme.ui.model;

import java.util.Date;

public interface RoomInterface {

    long getId();

    String getName();

    Date getUpdatedAt();

}