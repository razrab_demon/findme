package ru.panmin.findme.ui.base;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.panmin.findme.FindMeApplication;
import ru.panmin.findme.R;
import ru.panmin.findme.io.DataManager;
import ru.panmin.findme.utils.DialogFactory;
import ru.panmin.findme.utils.Utils;
import rx.Subscription;

public abstract class BaseActivity extends AppCompatActivity {

    @IdRes
    public static final int FRAGMENT_CONTAINER = 0;//R.id.container;

    protected DataManager dataManager;
    protected Subscription subscription;
    protected MaterialDialog progressDialog;
    @Nullable
    @BindView(R.id.rootView)
    protected View rootView;

    public BaseActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layoutRes = getLayoutRes();
        if (layoutRes != 0) {
            setContentView(layoutRes);
            ButterKnife.bind(this);
        }
        dataManager = FindMeApplication.get().getDataManager();
        FindMeApplication.BUS.register(this);
        init();
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void init();

    @Override
    public void onDestroy() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
        FindMeApplication.BUS.unregister(this);
        super.onDestroy();
    }

    protected void finishActivity() {
        hideKeyboard();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    protected void hideKeyboard() {
        View view = getRootView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected View getRootView() {
        if (findViewById(FRAGMENT_CONTAINER) != null) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(FRAGMENT_CONTAINER);
            if (fragment != null && fragment instanceof BaseFragment) {
                return ((BaseFragment) fragment).getRootView();
            } else {
                return findViewById(R.id.rootView);
            }
        } else {
            return findViewById(R.id.rootView);
        }
    }

    public void showProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        progressDialog = DialogFactory.createProgressDialog(this);
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showSnackbar(@NonNull String text) {
        View rootView = getRootView();
        if (rootView != null) {
            Utils.getShortSnackbar(rootView, text).show();
        } else {
            Utils.showShortToast(this, text);
        }
    }

    public void showSnackbar(@StringRes int text) {
        View rootView = getRootView();
        if (rootView != null) {
            Utils.getShortSnackbar(rootView, text).show();
        } else {
            Utils.showShortToast(this, text);
        }
    }

    @Subscribe
    public void onEvent(String gag) {
    }

}