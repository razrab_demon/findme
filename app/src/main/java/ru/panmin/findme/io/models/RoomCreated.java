package ru.panmin.findme.io.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import ru.panmin.findme.ui.model.RoomCreatedInterface;

public class RoomCreated extends Room implements Parcelable, RoomCreatedInterface {

    public static final Parcelable.Creator<RoomCreated> CREATOR = new Parcelable.Creator<RoomCreated>() {
        public RoomCreated createFromParcel(Parcel source) {
            return new RoomCreated(source);
        }

        public RoomCreated[] newArray(int size) {
            return new RoomCreated[size];
        }
    };

    @SerializedName("user_id")
    private long userId;

    @SerializedName("is_hide")
    private boolean isHide;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    public RoomCreated() {
    }

    private RoomCreated(Parcel in) {
        userId = in.readLong();
        isHide = in.readByte() == 1;
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isHide() {
        return isHide;
    }

    public void setHide(boolean hide) {
        isHide = hide;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(userId);
        dest.writeByte((byte) (isHide ? 1 : 0));
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

}