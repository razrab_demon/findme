package ru.panmin.findme.io.models;

import android.os.Parcel;
import android.os.Parcelable;

import ru.panmin.findme.ui.model.EditUserNameInterface;

public class EditUserName implements Parcelable, EditUserNameInterface {

    public static final Parcelable.Creator<EditUserName> CREATOR = new Parcelable.Creator<EditUserName>() {
        public EditUserName createFromParcel(Parcel source) {
            return new EditUserName(source);
        }

        public EditUserName[] newArray(int size) {
            return new EditUserName[size];
        }
    };

    public EditUserName() {
    }

    private EditUserName(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

}