package ru.panmin.findme.io.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import ru.panmin.findme.ui.model.AuthInterface;

public class Auth implements Parcelable, AuthInterface {

    public static final Parcelable.Creator<Auth> CREATOR = new Parcelable.Creator<Auth>() {
        public Auth createFromParcel(Parcel source) {
            return new Auth(source);
        }

        public Auth[] newArray(int size) {
            return new Auth[size];
        }
    };

    @SerializedName("user_id")
    private long id;

    @SerializedName("google_id")
    private String googleId;

    @SerializedName("email")
    private String email;

    @SerializedName("name")
    private String name;

    @SerializedName("token")
    private String token;

    @SerializedName("photo")
    private String photo;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    @SerializedName("created_at")
    private Date createdAt;

    public Auth() {
    }

    private Auth(Parcel in) {
        id = in.readLong();
        googleId = in.readString();
        email = in.readString();
        name = in.readString();
        token = in.readString();
        photo = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        long longCreatedAt = in.readLong();
        createdAt = longCreatedAt == -1 ? null : new Date(longCreatedAt);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(googleId);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(token);
        dest.writeString(photo);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeLong(createdAt == null ? -1 : createdAt.getTime());
    }

}