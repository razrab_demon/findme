package ru.panmin.findme.io.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import ru.panmin.findme.ui.model.RoomInterface;

public class Room implements Parcelable, RoomInterface {

    public static final Parcelable.Creator<Room> CREATOR = new Parcelable.Creator<Room>() {
        public Room createFromParcel(Parcel source) {
            return new Room(source);
        }

        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

    @SerializedName("room_id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("updated_at")
    private Date updatedAt;

    public Room() {
    }

    private Room(Parcel in) {
        id = in.readLong();
        name = in.readString();
        long longUpdatedAt = in.readLong();
        updatedAt = longUpdatedAt == -1 ? null : new Date(longUpdatedAt);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeLong(updatedAt == null ? -1 : updatedAt.getTime());
    }

}