package ru.panmin.findme.io.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import ru.panmin.findme.ui.model.RoomInListInterface;

public class RoomInList extends Room implements Parcelable, RoomInListInterface {

    public static final Parcelable.Creator<RoomInList> CREATOR = new Parcelable.Creator<RoomInList>() {
        public RoomInList createFromParcel(Parcel source) {
            return new RoomInList(source);
        }

        public RoomInList[] newArray(int size) {
            return new RoomInList[size];
        }
    };

    @SerializedName("password")
    private int password;

    @SerializedName("count_user")
    private int countUser;

    public RoomInList() {
    }

    private RoomInList(Parcel in) {
        password = in.readInt();
        countUser = in.readInt();
    }

    public int getPassword() {
        return password;
    }

    public boolean isPassword() {
        return password == 1;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public int getCountUser() {
        return countUser;
    }

    public void setCountUser(int countUser) {
        this.countUser = countUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(password);
        dest.writeInt(countUser);
    }

}