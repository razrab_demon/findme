package ru.panmin.findme.io;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.Date;

import ru.panmin.findme.io.models.Auth;

public class FindMePrefs {

    private static final String PREFS_FIND_ME_USER_ID = "find.me.user.id";
    private static final long DEFAULT_PREFS_FIND_ME_USER_ID = -1;

    private static final String PREFS_FIND_ME_USER_GOOGLE_ID = "find.me.user.google.id";
    private static final String DEFAULT_PREFS_FIND_ME_USER_GOOGLE_ID = "";

    private static final String PREFS_FIND_ME_USER_EMAIL = "find.me.user.email";
    private static final String DEFAULT_PREFS_FIND_ME_USER_EMAIL = "";

    private static final String PREFS_FIND_ME_USER_NAME = "find.me.user.name";
    private static final String DEFAULT_PREFS_FIND_ME_USER_NAME = "";

    private static final String PREFS_FIND_ME_TOKEN = "find.me.user.token";
    private static final String DEFAULT_PREFS_FIND_ME_TOKEN = "";

    private static final String PREFS_FIND_ME_USER_PHOTO = "find.me.user.photo";
    private static final String DEFAULT_PREFS_FIND_ME_USER_PHOTO = "";

    private static final String PREFS_FIND_ME_USER_LAT = "find.me.user.lat";
    private static final double DEFAULT_PREFS_FIND_ME_USER_LAT = 54.181506;

    private static final String PREFS_FIND_ME_USER_LNG = "find.me.user.lng";
    private static final double DEFAULT_PREFS_FIND_ME_USER_LNG = 45.181516;

    private static final String PREFS_FIND_ME_USER_CREATE_AT = "find.me.user.create.at";
    private static final long DEFAULT_PREFS_FIND_ME_USER_CREATE_AT = -1;

    public static void clear() {
        Prefs.clear();
    }

    public static void auth(Auth auth) {
        Prefs.putLong(PREFS_FIND_ME_USER_ID, auth.getId());
        Prefs.putString(PREFS_FIND_ME_USER_GOOGLE_ID, auth.getGoogleId());
        Prefs.putString(PREFS_FIND_ME_USER_EMAIL, auth.getEmail());
        Prefs.putString(PREFS_FIND_ME_USER_NAME, auth.getName());
        Prefs.putString(PREFS_FIND_ME_TOKEN, auth.getToken());
        Prefs.putString(PREFS_FIND_ME_USER_PHOTO, auth.getPhoto());
        Prefs.putDouble(PREFS_FIND_ME_USER_LAT, auth.getLat());
        Prefs.putDouble(PREFS_FIND_ME_USER_LNG, auth.getLng());
        Prefs.putLong(PREFS_FIND_ME_USER_CREATE_AT, auth.getCreatedAt() == null ? -1 : auth.getCreatedAt().getTime());
    }

    public static long getUserId() {
        return Prefs.getLong(PREFS_FIND_ME_USER_ID, DEFAULT_PREFS_FIND_ME_USER_ID);
    }

    public static String getUserGoogleId() {
        return Prefs.getString(PREFS_FIND_ME_USER_GOOGLE_ID, DEFAULT_PREFS_FIND_ME_USER_GOOGLE_ID);
    }

    public static String getUserEmail() {
        return Prefs.getString(PREFS_FIND_ME_USER_EMAIL, DEFAULT_PREFS_FIND_ME_USER_EMAIL);
    }

    public static String getUserName() {
        return Prefs.getString(PREFS_FIND_ME_USER_NAME, DEFAULT_PREFS_FIND_ME_USER_NAME);
    }

    public static String getToken() {
        return Prefs.getString(PREFS_FIND_ME_TOKEN, DEFAULT_PREFS_FIND_ME_TOKEN);
    }

    public static String getUserPhoto() {
        return Prefs.getString(PREFS_FIND_ME_USER_PHOTO, DEFAULT_PREFS_FIND_ME_USER_PHOTO);
    }

    public static double getUserLat() {
        return Prefs.getDouble(PREFS_FIND_ME_USER_LAT, DEFAULT_PREFS_FIND_ME_USER_LAT);
    }

    public static double getUserLng() {
        return Prefs.getDouble(PREFS_FIND_ME_USER_LNG, DEFAULT_PREFS_FIND_ME_USER_LNG);
    }

    public static Date getUserCreateAt() {
        long longCreatedAt = Prefs.getLong(PREFS_FIND_ME_USER_CREATE_AT, DEFAULT_PREFS_FIND_ME_USER_CREATE_AT);
        return longCreatedAt == -1 ? null : new Date(longCreatedAt);
    }

}