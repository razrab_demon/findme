package ru.panmin.findme.io;

public class DataManager {

    private FindMeService findMeService;

    public DataManager(FindMeService findMeService) {
        this.findMeService = findMeService;
    }

    public FindMeService getFindMeService() {
        return findMeService;
    }

}