package ru.panmin.findme.io.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.panmin.findme.ui.model.BaseListInterface;

public class BaseList<T> implements BaseListInterface<T> {

    @SerializedName("list")
    private List<T> list = new ArrayList<>();

    @SerializedName("count")
    private int count;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}