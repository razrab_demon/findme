package ru.panmin.findme.io;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import ru.panmin.findme.BuildConfig;
import ru.panmin.findme.io.responses.AuthResponse;
import ru.panmin.findme.io.responses.EditUserNameResponse;
import ru.panmin.findme.io.responses.RoomCreateResponse;
import ru.panmin.findme.io.responses.RoomListResponse;
import rx.Observable;

public interface FindMeService {

    String ENDPOINT = "http://findme.panmin.ru/";

    @FormUrlEncoded
    @POST("api/auth")
    Observable<AuthResponse> auth(
            @Field("email") String email,
            @Field("google_id") String googleId,
            @Field("name") String name
    );

    @PUT("api/user")
    Observable<EditUserNameResponse> editUserName(
            @Field("name") String name
    );

    @GET("api/room")
    Observable<RoomListResponse> roomList(
            @Query("search") String search
    );

    @FormUrlEncoded
    @POST("api/room")
    Observable<RoomCreateResponse> createRoom(
            @Field("name") String name,
            @Field("password") String password,
            @Field("is_hide") boolean is_hide
    );

    class Creator {

        public static FindMeService newService() {
            OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                okBuilder.addInterceptor(interceptor).build();
            }

            Interceptor authInterceptor = chain -> {
                Request original = chain.request();
                String token = FindMePrefs.getToken();
                Request.Builder requestBuilder = original.newBuilder();
                if (!TextUtils.isEmpty(token)) {
                    requestBuilder
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .header("X-Auth-Token", token)
                            .method(original.method(), original.body());
                }
                Request request = requestBuilder.build();
                return chain.proceed(request);
            };
            okBuilder.addInterceptor(authInterceptor);

            OkHttpClient client = okBuilder.build();

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(FindMeService.ENDPOINT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            return retrofit.create(FindMeService.class);
        }

    }

}