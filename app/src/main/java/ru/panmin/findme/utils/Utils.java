package ru.panmin.findme.utils;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ru.panmin.findme.R;

public class Utils {

    public static Snackbar getShortSnackbar(View rootView, @NonNull String text) {
        return getSnackbar(rootView, text, Snackbar.LENGTH_SHORT);
    }

    public static Snackbar getShortSnackbar(View rootView, @StringRes int text) {
        return getSnackbar(rootView, rootView.getResources().getString(text), Snackbar.LENGTH_SHORT);
    }

    public static Snackbar getLongSnackbar(View rootView, @NonNull String text) {
        return getSnackbar(rootView, text, Snackbar.LENGTH_LONG);
    }

    public static Snackbar getLongSnackbar(View rootView, @StringRes int text) {
        return getSnackbar(rootView, rootView.getResources().getString(text), Snackbar.LENGTH_LONG);
    }

    public static Snackbar getIndefiniteSnackbar(View rootView, @NonNull String text) {
        return getSnackbar(rootView, text, Snackbar.LENGTH_INDEFINITE);
    }

    public static Snackbar getIndefiniteSnackbar(View rootView, @StringRes int text) {
        return getSnackbar(rootView, rootView.getResources().getString(text), Snackbar.LENGTH_INDEFINITE);
    }

    private static Snackbar getSnackbar(View rootView, @NonNull String text, int length) {
        Snackbar snackbar = Snackbar.make(rootView, text, length);//.setActionTextColor(ContextCompat.getColor(rootView.getContext(), R.color.snackbar_color_action));
        View snackbarView = snackbar.getView();
        //snackbarView.setBackgroundResource(R.color.snackbar_color_background);
        TextView tv = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        //tv.setTextColor(ContextCompat.getColor(rootView.getContext(), R.color.snackbar_color_text));
        tv.setMaxLines(Integer.MAX_VALUE);
        return snackbar;
    }

    public static void showShortToast(Context context, @NonNull String text) {
        showToast(context, text, Toast.LENGTH_SHORT);
    }

    public static void showShortToast(Context context, @StringRes int text) {
        showToast(context, context.getResources().getString(text), Toast.LENGTH_SHORT);
    }

    public static void showLongToast(Context context, @NonNull String text) {
        showToast(context, text, Toast.LENGTH_LONG);
    }

    public static void showLongToast(Context context, @StringRes int text) {
        showToast(context, context.getResources().getString(text), Toast.LENGTH_LONG);
    }

    private static void showToast(Context context, String text, int length) {
        Toast.makeText(context, text, length).show();
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}