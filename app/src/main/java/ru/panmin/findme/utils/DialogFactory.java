package ru.panmin.findme.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import ru.panmin.findme.R;

public class DialogFactory {

    public static MaterialDialog createProgressDialog(Context context, String message) {
        return new MaterialDialog.Builder(context)
                .content(message)
                .cancelable(false)
                .progress(true, 0)
                .build();
    }

    public static MaterialDialog createProgressDialog(Context context, @StringRes int messageResource) {
        return new MaterialDialog.Builder(context)
                .content(messageResource)
                .cancelable(false)
                .progress(true, 0)
                .build();
    }

    public static MaterialDialog createProgressDialog(Context context) {
        return new MaterialDialog.Builder(context)
                .content(context.getResources().getString(R.string.dialog_progress_default))
                .cancelable(false)
                .progress(true, 0)
                .build();
    }

    public static MaterialDialog needLocationPermissionDialog(Context context, MaterialDialog.SingleButtonCallback callback) {
        return new MaterialDialog.Builder(context)
                .title(context.getResources().getString(R.string.wanted))
                .content(context.getResources().getString(R.string.need_location_permission))
                .positiveText(context.getResources().getString(R.string.ok))
                .onPositive(callback)
                .cancelable(false)
                .build();
    }

    public static MaterialDialog createRoomCreateDialog(Context context, View view, MaterialDialog.SingleButtonCallback callback) {
        return new MaterialDialog.Builder(context)
                .customView(view, true)
                .negativeText(context.getResources().getString(R.string.cancel))
                .positiveText(context.getResources().getString(R.string.ok))
                .cancelable(true)
                .onPositive(callback)
                .onNegative((dialog, which) -> dialog.dismiss())
                .autoDismiss(false)
                .build();
    }

}